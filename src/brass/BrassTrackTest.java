package brass;

import org.junit.*;
import static org.junit.Assert.*;

public class BrassTrackTest
{
	private BrassTrack bt;
	private int allowed_error;
	private BrassCottonDemandTrack bcdt;
	
	
	@Before 
	public void setUp() 
	{
		BrassXML bx = new BrassXML("resources/brass_pixels.xml");
		bt = new BrassTrack(bx);
		allowed_error = 2;  //+- two pixels is the allowed error
		bcdt = new BrassCottonDemandTrack(bx);
		
    }
	
	
	
	
	

	/*
	===============================TEST for brassCottonDemand==============================================
	*/
	@Test
	public void brassCottonDemand()
	{
		//moves the marker down and returns extra income
		int cotton_demand_extra_income = bcdt.cottonTrackIncome();
		//make sure marker is in correct location
		int cotton_demand_index = bcdt.getCottonDemandIndex();
		
		assertTrue("At T1 cotton_demand_index: TOO LOW", 1 < cotton_demand_index);
		assertTrue("At T1 cotton_demand_index: TOO HIGH", 3 > cotton_demand_index);
		assertTrue("At T1 cotton_demand_extra_income: TOO LOW", 1 < cotton_demand_extra_income);
		assertTrue("At T1 cotton_demand_extra_income: TOO HIGH", 3 > cotton_demand_extra_income);
		
		
		cotton_demand_extra_income = bcdt.cottonTrackIncome();
		cotton_demand_index = bcdt.getCottonDemandIndex();
		
		assertTrue("At T2 cotton_demand_index: TOO LOW", 2 < cotton_demand_index);
		assertTrue("At T2 cotton_demand_index: TOO HIGH", 4 > cotton_demand_index);
		assertTrue("At T2 cotton_demand_extra_income: TOO LOW", 1 < cotton_demand_extra_income);
		assertTrue("At T2 cotton_demand_extra_income: TOO HIGH", 3 > cotton_demand_extra_income);
		
		
		cotton_demand_extra_income = bcdt.cottonTrackIncome();
		cotton_demand_index = bcdt.getCottonDemandIndex();
		
		assertTrue("At T3 cotton_demand_index: TOO LOW", 5 < cotton_demand_index);
		assertTrue("At T3 cotton_demand_index: TOO HIGH", 7 > cotton_demand_index);
		assertTrue("At T3 cotton_demand_extra_income: TOO LOW", -1 < cotton_demand_extra_income);
		assertTrue("At T3 cotton_demand_extra_income: TOO HIGH", 1 > cotton_demand_extra_income);
		
		
		cotton_demand_extra_income = bcdt.cottonTrackIncome();
		cotton_demand_index = bcdt.getCottonDemandIndex();
		
		assertTrue("At T4 cotton_demand_index: TOO LOW", 7 < cotton_demand_index);
		assertTrue("At T4 cotton_demand_index: TOO HIGH", 9 > cotton_demand_index);
		assertTrue("At T4 cotton_demand_extra_income: TOO LOW", -2 < cotton_demand_extra_income);
		assertTrue("At T4 cotton_demand_extra_income: TOO HIGH", 0 > cotton_demand_extra_income);
		
		
		cotton_demand_extra_income = bcdt.cottonTrackIncome();
		cotton_demand_index = bcdt.getCottonDemandIndex();
		
		assertTrue("At T5 cotton_demand_index: TOO LOW", 7 < cotton_demand_index);
		assertTrue("At T5 cotton_demand_index: TOO HIGH", 9 > cotton_demand_index);
		assertTrue("At T5 cotton_demand_extra_income: TOO LOW", -2 < cotton_demand_extra_income);
		assertTrue("At T5 cotton_demand_extra_income: TOO HIGH", 0 > cotton_demand_extra_income);
	}
	
	/*
	===============================TEST for brassLocTrackTest==============================================
	*/
	//(x, y) for location 0 is specified in brass_pixels.xml
	//as it doesn't depend on the offsets, this test should certainly pass
	@Test
	public void brassLocTrackTest0()
	{
		int x_pos = bt.getXPixel(0);
		int y_pos = bt.getYPixel(0);
		assertTrue("Brass Track Loc X 0: TOO LOW", 322 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 322 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 642 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 1 is (302, 642)
	@Test
	public void brassLocTrackTest1() 
	{
		int x_pos = bt.getXPixel(1);
		int y_pos = bt.getYPixel(1);
		assertTrue("Brass Track Loc X 1: TOO LOW", 302 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 1: TOO HIGH", 302 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 1: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 1: TOO HIGH", 642 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 5 is (226, 642)
	@Test
	public void brassLocTrackTest5() 
	{
		int x_pos = bt.getXPixel(5);
		int y_pos = bt.getYPixel(5);
		assertTrue("Brass Track Loc X 5: TOO LOW", 226 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 5: TOO HIGH", 226 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 5: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 5: TOO HIGH", 642 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 10 is (132, 642)
	@Test
	public void brassLocTrackTest10() 
	{
		int x_pos = bt.getXPixel(10);
		int y_pos = bt.getYPixel(10);
		assertTrue("Brass Track Loc X 10: TOO LOW", 132 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 10: TOO HIGH", 132 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 10: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 10: TOO HIGH", 642 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 16 is (20, 642)
	@Test
	public void brassLocTrackTest16() 
	{
		int x_pos = bt.getXPixel(16);
		int y_pos = bt.getYPixel(16);
		assertTrue("Brass Track Loc X 16: TOO LOW", 20 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 16: TOO HIGH", 20 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 16: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 16: TOO HIGH", 642 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 17 is (20, 623)
	@Test
	public void brassLocTrackTest17() 
	{
		int x_pos = bt.getXPixel(17);
		int y_pos = bt.getYPixel(17);
		assertTrue("Brass Track Loc X 17: TOO LOW", 20 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 17: TOO HIGH", 20 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 17: TOO LOW", 623 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 17: TOO HIGH", 623 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 18 is (20, 605)
	@Test
	public void brassLocTrackTest18() 
	{
		int x_pos = bt.getXPixel(18);
		int y_pos = bt.getYPixel(18);
		assertTrue("Brass Track Loc X 18: TOO LOW", 20 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 18: TOO HIGH", 20 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 18: TOO LOW", 605 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 18: TOO HIGH", 605 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 32 is (245, 568)
	@Test
	public void brassLocTrackTest32() 
	{
		int x_pos = bt.getXPixel(32);
		int y_pos = bt.getYPixel(32);
		assertTrue("Brass Track Loc X 32: TOO LOW", 245 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 32: TOO HIGH", 245 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 32: TOO LOW", 568 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 32: TOO HIGH", 568 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 46 is (19, 532)
	@Test
	public void brassLocTrackTest46() 
	{
		int x_pos = bt.getXPixel(46);
		int y_pos = bt.getYPixel(46);
		assertTrue("Brass Track Loc X 46: TOO LOW", 19 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 46: TOO HIGH", 19 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 46: TOO LOW", 532 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 46: TOO HIGH", 532 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 55 is (151, 494)
	@Test
	public void brassLocTrackTest55() 
	{
		int x_pos = bt.getXPixel(55);
		int y_pos = bt.getYPixel(55);
		assertTrue("Brass Track Loc X 55: TOO LOW", 151 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 55: TOO HIGH", 151 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 55: TOO LOW", 494 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 55: TOO HIGH", 494 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 64 is (19, 458)
	@Test
	public void brassLocTrackTest64() 
	{
		int x_pos = bt.getXPixel(64);
		int y_pos = bt.getYPixel(64);
		assertTrue("Brass Track Loc X 64: TOO LOW", 19 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 64: TOO HIGH", 19 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 64: TOO LOW", 458 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 64: TOO HIGH", 458 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 70 is (94, 421)
	@Test
	public void brassLocTrackTest70() 
	{
		int x_pos = bt.getXPixel(70);
		int y_pos = bt.getYPixel(70);
		assertTrue("Brass Track Loc X 70: TOO LOW", 94 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 70: TOO HIGH", 94 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 70: TOO LOW", 421 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 70: TOO HIGH", 421 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 76 is (19, 386)
	@Test
	public void brassLocTrackTest76() 
	{
		int x_pos = bt.getXPixel(76);
		int y_pos = bt.getYPixel(76);
		assertTrue("Brass Track Loc X 76: TOO LOW",  19 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 76: TOO HIGH", 19 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 76: TOO LOW", 386 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 76: TOO HIGH", 386 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 82 is (94, 349)
	@Test
	public void brassLocTrackTest82() 
	{
		int x_pos = bt.getXPixel(82);
		int y_pos = bt.getYPixel(82);
		assertTrue("Brass Track Loc X 82: TOO LOW", 94 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 82: TOO HIGH", 94 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 82: TOO LOW", 349 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 82: TOO HIGH", 349 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 88 is (18, 311)
	@Test
	public void brassLocTrackTest88() 
	{
		int x_pos = bt.getXPixel(88);
		int y_pos = bt.getYPixel(88);
		assertTrue("Brass Track Loc X 88: TOO LOW", 18 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 88: TOO HIGH", 18 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 88: TOO LOW", 311 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 88: TOO HIGH", 311 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 93 is (74, 275)
	@Test
	public void brassLocTrackTest93() 
	{
		int x_pos = bt.getXPixel(93);
		int y_pos = bt.getYPixel(93);
		assertTrue("Brass Track Loc X 93: TOO LOW", 74 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 93: TOO HIGH", 74 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 93: TOO LOW", 275 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 93: TOO HIGH", 275 >= (y_pos - allowed_error));
	}
	
	//(x, y) for location 98 is (18, 238)
	@Test
	public void brassLocTrackTest98() 
	{
		int x_pos = bt.getXPixel(98);
		int y_pos = bt.getYPixel(98);
		assertTrue("Brass Track Loc X 98: TOO LOW", 18 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 98: TOO HIGH", 18 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 98: TOO LOW", 238 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 98: TOO HIGH", 238 >= (y_pos - allowed_error));
	}
	
	/*
	===============================TESTS for getIncomeAmount==============================================
	*/

	@Test
	public void getIncomeAmountTest7() 
	{
		int value = bt.getIncomeAmount(0);
		assertTrue("Brass Track Take Loan 50: TOO LOW", -11 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", -9 > value);
	}
	
	@Test
	public void getIncomeAmountTest8() 
	{
		int value = bt.getIncomeAmount(2);
		assertTrue("Brass Track Take Loan 50: TOO LOW", -9 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", -7 > value);
	}
	
	@Test
	public void getIncomeAmountTest9() 
	{
		int value = bt.getIncomeAmount(5);
		assertTrue("Brass Track Take Loan 50: TOO LOW", -6 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", -4 > value);
	}
	
	@Test
	public void getIncomeAmountTest10() 
	{
		int value = bt.getIncomeAmount(7);
		assertTrue("Brass Track Take Loan 50: TOO LOW", -4 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", -2 > value);
	}
	
	@Test
	public void getIncomeAmountTest11() 
	{
		int value = bt.getIncomeAmount(9);
		assertTrue("Brass Track Take Loan 50: TOO LOW", -2 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 0 > value);
	}
	
	@Test
	public void getIncomeAmountTest2() 
	{
		int value = bt.getIncomeAmount(31);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 10 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 12 > value);
	}
	
	@Test
	public void getIncomeAmountTest3() 
	{
		int value = bt.getIncomeAmount(39);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 12 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 14 > value);
	}

	@Test
	public void getIncomeAmountTest4() 
	{
		int value = bt.getIncomeAmount(44);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 14 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 16 > value);
	}
	
	@Test
	public void getIncomeAmountTest1() 
	{
		int value = bt.getIncomeAmount(50);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 16 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 18 > value);
	}

	@Test
	public void getIncomeAmountTest5() 
	{
		int value = bt.getIncomeAmount(64);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 20 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 22 > value);
	}
	
	@Test
	public void getIncomeAmountTest6() 
	{
		int value = bt.getIncomeAmount(72);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 22 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 24 > value);
	}
	
	@Test
	public void getIncomeAmountTest12() 
	{
		int value = bt.getIncomeAmount(99);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 29 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 31 > value);
	}
	
	/*
	===============================TESTS FOR takeLoan method==============================================
	*/
	@Test
	public void takeLoanTest3() 
	{
		int value = bt.takeLoan(16, 10);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 13 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 15 > value);
	}
	
	@Test
	public void takeLoanTest7() 
	{
		int value = bt.takeLoan(39, 30);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 29 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 31 > value);
	}
	
	@Test
	public void takeLoanTest2() 
	{
		int value = bt.takeLoan(43, 20);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 38 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 40 > value);
	}
	
	@Test
	public void takeLoanTest6() 
	{
		int value = bt.takeLoan(67, 10);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 63 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 65 > value);
	}
	
	@Test
	public void takeLoanTest5() 
	{
		int value = bt.takeLoan(78, 20);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 71 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 73 > value);
	}
	
	@Test
	public void takeLoanTest1() 
	{
		int value = bt.takeLoan(50, 10);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 47 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 49 > value);
	}
	
	@Test
	public void takeLoanTest4() 
	{
		int value = bt.takeLoan(99, 30);
		assertTrue("Brass Track Take Loan 50: TOO LOW", 87 < value);
		assertTrue("Brass Track Take Loan 50: TOO HIGH", 89 > value);
	}

}
